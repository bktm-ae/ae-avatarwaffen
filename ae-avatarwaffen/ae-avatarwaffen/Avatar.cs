﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ae_avatarwaffen
{
    class Avatar
    {
        public string Name { get; set; }
        public int Hp { get; set; }

        internal Waffe myWaffe { get; set; } //Speicherplatz für eine Waffe

        public Avatar(string Name, int Hp, Waffe w)
        {
            this.Name = Name;
            this.Hp = Hp;
            myWaffe = w;
        }

        public Avatar(Waffe w)
        {
            myWaffe = w;
        }
    }
}
