﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ae_avatarwaffen
{
    class Program
    {
        static void Main(string[] args)
        {
            //Aufruf standard Konstruktor
            Waffe w1 = new Waffe();
            w1.Name = "Oathkeeper";
            w1.Schaden = 69;

            Avatar a1 = new Avatar(w1);

            a1.Name = "Brianne";
            a1.Hp = 100;

            Console.WriteLine(a1.Name + " hat " + a1.Hp + " Lebenspunkte und die Waffe: " 
                + a1.myWaffe.Name + " mit " + a1.myWaffe.Schaden + " DMG.");

            Console.ReadLine();
        }
    }
}
